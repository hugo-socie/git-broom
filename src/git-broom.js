import { exec } from 'child_process';
import prompts from 'prompts';

export function cli() {
  this
    .execute('git branch | tr -d "*" | tr -d " "')
    .then((response) => {
      const branchs = response.split('\n');

      asyncForEach(branchs, async (branch) => {
        if (branch) {
          const response = await prompts({
            type: 'toggle',
            name: 'value',
            message: `Delete local branch ${branch} ?`,
            initial: true,
            active: 'yes',
            inactive: 'nop',
          });
          if (response.value) execute(`git branch -D ${branch}`);
        }
      });
    });
}

export function execute(command, print = false) {
  return new Promise((resolve, reject) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        if (print) console.log(error.message);
        reject(error.message);
      } else if (stderr) {
        if (print) console.log(stderr);
        reject(stderr)
      } else {
        if (print) console.log(stdout);
        resolve(stdout);
      }
    });
  });
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
